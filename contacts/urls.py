from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('<contact_id>', views.show_contact, name = 'show_contact'),
]
