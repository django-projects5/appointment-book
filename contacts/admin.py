from django.contrib import admin
from .models import Category, Contact

# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'number_phone',
                    'email', 'created_at', 'category')
    list_display_links = ('id', 'first_name', 'last_name')
    list_filter = ('first_name', 'last_name')
    list_per_page = 10
    search_fields = ('first_name', 'last_name', 'number_phone')

admin.site.register(Category)
admin.site.register(Contact, ContactAdmin)
